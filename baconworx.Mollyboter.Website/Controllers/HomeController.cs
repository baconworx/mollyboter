﻿using Microsoft.AspNet.Mvc;

namespace baconworx.Mollyboter.Website.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        [Route("/")]
        [Route("/Index")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/About")]
        public IActionResult About()
        {
            return View();
        }

        [Route("/Contact")]
        public IActionResult Contact()
        {
            return View();
        }

        [Route("/Oauth")]
        public IActionResult Oauth() => View();

        [Route("/Error")]
        public IActionResult Error()
        {
            return View();
        }
    }
}
