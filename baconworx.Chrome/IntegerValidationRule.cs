﻿using System.Windows.Controls;

namespace baconworx.Chrome
{
    public class IntegerValidationRule : ValidationRule
    {

        public string PropertyNameToDisplay { get; set; }
        public bool Nullable { get; set; }
        public bool AllowNegative { get; set; }

        string propertyNameHelper => PropertyNameToDisplay == null ? string.Empty : " for " + PropertyNameToDisplay;

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var textEntered = (string) value;
            int intOutput;
            double junkd;

            if (string.IsNullOrEmpty(textEntered))
                return Nullable
                    ? new ValidationResult(true, null)
                    : new ValidationResult(false, getMsgDisplay("Please enter a value"));

            if (!int.TryParse(textEntered, out intOutput))
                return double.TryParse(textEntered, out junkd)
                    ? new ValidationResult(false, getMsgDisplay("Please enter a whole number (no decimals)"))
                    : new ValidationResult(false, getMsgDisplay("Please enter a whole number"));

            return intOutput < 0 && !AllowNegative
                ? new ValidationResult(false, getNegativeNumberError())
                : new ValidationResult(true, null);
        }

        private string getNegativeNumberError()
        {
            return PropertyNameToDisplay == null
                ? "This property must be a positive, whole number"
                : $"{PropertyNameToDisplay} must be a positive, whole number";
        }

        private string getMsgDisplay(string messageBase)
        {
            return $"{messageBase}{propertyNameHelper}";
        }
    }
}
