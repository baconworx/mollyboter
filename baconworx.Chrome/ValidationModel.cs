﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace baconworx.Chrome
{
    public abstract class ValidationModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        #region Fields 

        private readonly Dictionary<string, object> _values = new Dictionary<string, object>();
        private readonly Dictionary<string, string> _asyncErrors = new Dictionary<string, string>();

        private static readonly object _valuesLock = new object();

        #endregion

        #region Protected 

        /// <summary> 
        /// Sets the value of a property. 
        /// </summary> 
        /// <typeparam name="T">The type of the property value.</typeparam> 
        /// <param name="propertySelector">Expression tree contains the property definition.</param> 
        /// <param name="value">The property value.</param> 
        protected void SetValue<T>(Expression<Func<T>> propertySelector, T value)
        {
            var propertyName = GetPropertyName(propertySelector);

            SetValue(propertyName, value);
        }

        /// <summary> 
        /// Sets the value of a property. 
        /// </summary> 
        /// <typeparam name="T">The type of the property value.</typeparam> 
        /// <param name="propertyName">The name of the property.</param> 
        /// <param name="value">The property value.</param> 
        protected void SetValue<T>(string propertyName, T value)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("Invalid property name", propertyName);
            }

            _values[propertyName] = value;
            NotifyPropertyChanged(propertyName);
        }

        /// <summary> 
        /// Gets the value of a property. 
        /// </summary> 
        /// <typeparam name="T">The type of the property value.</typeparam> 
        /// <param name="propertySelector">Expression tree contains the property definition.</param> 
        /// <returns>The value of the property or default value if not exist.</returns> 
        protected T GetValue<T>(Expression<Func<T>> propertySelector)
        {
            var propertyName = GetPropertyName(propertySelector);

            return GetValue<T>(propertyName);
        }

        /// <summary> 
        /// Gets the value of a property. 
        /// </summary> 
        /// <typeparam name="T">The type of the property value.</typeparam> 
        /// <param name="propertyName">The name of the property.</param> 
        /// <returns>The value of the property or default value if not exist.</returns> 
        protected T GetValue<T>(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("Invalid property name", propertyName);
            }

            object value;
            if (!_values.TryGetValue(propertyName, out value))
            {
                value = default(T);
                _values.Add(propertyName, value);
            }

            return (T)value;
        }

        /// <summary> 
        /// Validates current instance properties using Data Annotations. 
        /// </summary> 
        /// <param name="propertyName">This instance property to validate.</param> 
        /// <returns>Relevant error string on validation failure or <see cref="System.String.Empty"/> on validation success.</returns> 
        protected virtual string OnValidate(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                throw new ArgumentException("Invalid property name", propertyName);

            var error = string.Empty;
            var value = getValue(propertyName);
            var results = new List<ValidationResult>(1);
            var result = Validator.TryValidateProperty(value, new ValidationContext(this, null, null) { MemberName = propertyName }, results);

            if (result)
                return error;

            var validationResult = results.First();
            error = validationResult.ErrorMessage;

            return error;
        }

        #endregion

        #region Change Notification 

        /// <summary> 
        /// Raised when a property on this object has a new value. 
        /// </summary> 
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary> 
        /// Raises this object's PropertyChanged event. 
        /// </summary> 
        /// <param name="propertyName">The property that has a new value.</param> 
        protected void NotifyPropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);

            var handler = PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }

            ValidateAsync();

        }

        public void ValidateAsync()
        {
            AsyncValidator.TryValidateObject(this, new ValidationContext(this), validationResult =>
              {
                  var prop = validationResult.MemberNames.First();

                  if (string.IsNullOrEmpty(validationResult.ErrorMessage))
                      _asyncErrors.Remove(prop);
                  else
                      _asyncErrors[validationResult.MemberNames.First()] = validationResult.ErrorMessage;

                  NotifyErrorsChanged(prop);

              });
        }

        protected void NotifyPropertyChanged<T>(Expression<Func<T>> propertySelector)
        {
            NotifyPropertyChanged(GetPropertyName(propertySelector));
        }

        #endregion // INotifyPropertyChanged Members 

        #region Error Notification 

        /// <summary> 
        /// Raised when a property on this object has a new value. 
        /// </summary> 
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <summary> 
        /// Raises this object's PropertyChanged event. 
        /// </summary> 
        /// <param name="propertyName">The property that has a new value.</param> 
        protected void NotifyErrorsChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);

            var handler = ErrorsChanged;
            if (handler == null) return;

            var e = new DataErrorsChangedEventArgs(propertyName);
            handler(this, e);
        }

        protected void NotifyErrorsChanged<T>(Expression<Func<T>> propertySelector)
        {
            NotifyErrorsChanged(GetPropertyName(propertySelector));
        }
        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                return null;

            var errorMessage = OnValidate(propertyName);

            if (errorMessage == string.Empty)
                errorMessage = _asyncErrors.ContainsKey(propertyName) ? _asyncErrors[propertyName] : string.Empty;

            return string.IsNullOrEmpty(errorMessage) ? new string[] { } : new[] { errorMessage };
        }

        public bool HasErrors => _asyncErrors.Count > 0;

        #endregion // INotifyPropertyChanged Members 

        #region Data Validation 

        public string Error => string.Empty;

        public string this[string propertyName]
        {
            get
            {
                var result = OnValidate(propertyName);

                if (result == string.Empty)
                    return _asyncErrors.ContainsKey(propertyName) ? _asyncErrors[propertyName] : string.Empty;

                return result;
            }
        }

        #endregion

        #region Privates 

        private string GetPropertyName(LambdaExpression expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new InvalidOperationException();

            return memberExpression.Member.Name;
        }

        private object getValue(string propertyName)
        {
            lock (_valuesLock)
            {
                object value;
                if (_values.TryGetValue(propertyName, out value))
                    return value;

                var propertyDescriptor = TypeDescriptor.GetProperties(GetType()).Find(propertyName, false);
                if (propertyDescriptor == null)
                    throw new ArgumentException("Invalid property name", propertyName);

                value = propertyDescriptor.GetValue(this);
                _values.Add(propertyName, value);

                return value;
            }
        }

        #endregion

        #region Debugging 

        /// <summary> 
        /// Warns the developer if this object does not have 
        /// a public property with the specified name. This  
        /// method does not exist in a Release build. 
        /// </summary> 
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,   
            // public, instance property on this object. 
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                var msg = "Invalid property name: " + propertyName;

                if (ThrowOnInvalidPropertyName)
                    throw new Exception(msg);

                Debug.Fail(msg);
            }
        }

        /// <summary> 
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used 
        /// when an invalid property name is passed to the VerifyPropertyName method. 
        /// The default value is false, but subclasses used by unit tests might  
        /// override this property's getter to return true. 
        /// </summary> 
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; } = false;

        #endregion // Debugging Aides 

    }
}