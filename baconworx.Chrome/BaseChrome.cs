﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using fasm.Chrome;

namespace baconworx.Chrome
{
    public abstract class BaseChrome : Window
    {
        private FrameworkElement _borderBottom;
        private FrameworkElement _borderBottomLeft;
        private FrameworkElement _borderBottomRight;
        private FrameworkElement _borderLeft;
        private FrameworkElement _borderRight;
        private FrameworkElement _borderTopLeft;
        private FrameworkElement _borderTopRight;
        private FrameworkElement _caption;
        private Point _cursorOffset;
        private FrameworkElement _frame;
        private IntPtr _handle;
        private Image _minimizeButton;
        private Image _maximizeButton;
        private Image _closeButton;
        private double _restoreTop;

        private BitmapImage _maxButtonOn, _maxButtonOff;
        private BitmapImage _closeButtonOn, _closeButtonOff;
        private BitmapImage _minButtonOn, _minButtonOff;

        private IntPtr _nextClipboardViewer;

        public static readonly DependencyProperty IsDialogProperty = DependencyProperty.Register("IsDialog", typeof(bool), typeof(BaseChrome), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty HasOkButtonProperty = DependencyProperty.Register("HasOkButton", typeof(bool), typeof(BaseChrome), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty HasCancelButtonProperty = DependencyProperty.Register("HasCancelButton", typeof(bool), typeof(BaseChrome), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty HasApplyButtonProperty = DependencyProperty.Register("HasApplyButton", typeof(bool), typeof(BaseChrome), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty ShadowColorProperty = DependencyProperty.Register("ShadowColor", typeof(Color), typeof(BaseChrome), new FrameworkPropertyMetadata(Colors.Black));

        public bool IsDialog { get { return (bool)GetValue(IsDialogProperty); } set { SetValue(IsDialogProperty, value); } }
        public bool HasOkButton { get { return (bool)GetValue(HasOkButtonProperty); } set { SetValue(HasOkButtonProperty, value); } }
        public bool HasCancelButton { get { return (bool)GetValue(HasCancelButtonProperty); } set { SetValue(HasCancelButtonProperty, value); } }
        public bool HasApplyButton { get { return (bool)GetValue(HasApplyButtonProperty); } set { SetValue(HasApplyButtonProperty, value); } }
        public Color ShadowColor { get { return (Color)GetValue(ShadowColorProperty); } set { SetValue(ShadowColorProperty, value); } }

        public FrameworkElement BorderTop { get; set; }

        public bool IsValid => isValid(this);

        protected event EventHandler clipboardTextChanged;

        protected BaseChrome()
        {
            addResourceDictionary("baconworx.Chrome;component/BaseChromeStyle.xaml");

            SourceInitialized += (sender, e) =>
            {
                _handle = new WindowInteropHelper(this).Handle;
                // ReSharper disable once PossibleNullReferenceException
                HwndSource.FromHwnd(_handle).AddHook(wndProc);

                if (!IsDialog)
                    _nextClipboardViewer = NativeMethods.SetClipboardViewer(_handle);
                else
                    ShowInTaskbar = false;
            };

            Closed += (sender, args) =>
            {
                if (!IsDialog) NativeMethods.ChangeClipboardChain(_handle, _nextClipboardViewer);
                removeResourceDictionary("baconworx.Chrome;component/BaseChromeStyle.xaml");
            };

            Style = (Style)TryFindResource("BaseChromeStyle");
            IsDialog = false;
        }

        private static void addResourceDictionary(string source)
        {
            ResourceDictionary resourceDictionary = Application.LoadComponent(new Uri(source, UriKind.RelativeOrAbsolute)) as ResourceDictionary;
            Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
        }

        private static void removeResourceDictionary(string source)
        {
            ResourceDictionary resourceDictionary = Application.LoadComponent(new Uri(source, UriKind.RelativeOrAbsolute)) as ResourceDictionary;
            Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
        }


        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            registerFrame();
            registerBorders();
            registerCaption();
            registerMinimizeButton();
            registerMaximizeButton();
            registerCloseButton();
            registerOkCancelApplyButtons();
        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        private void registerOkCancelApplyButtons()
        {
            var okButton = (Button)GetTemplateChild("PART_BottomButtonsOk");
            var cancelButton = (Button)GetTemplateChild("PART_BottomButtonsCancel");
            var applyButton = (Button)GetTemplateChild("PART_BottomButtonsApply");

            okButton.Click += OnOkButtonClick;
            cancelButton.Click += OnCancelButtonClick;
            applyButton.Click += OnApplyButtonClick;
        }

        protected virtual void OnOkButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (!OnValidate())
                return;

            try { DialogResult = true; } catch { /* ignored */ }

            Close();
        }

        protected virtual bool OnValidate()
        {
            return IsValid;
        }

        protected virtual void OnCancelButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            Close();
        }

        protected virtual void OnApplyButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
        }

        private void registerBorders()
        {
            _borderLeft = (FrameworkElement)GetTemplateChild("PART_WindowBorderLeft");
            _borderTopLeft = (FrameworkElement)GetTemplateChild("PART_WindowBorderTopLeft");
            BorderTop = (FrameworkElement)GetTemplateChild("PART_WindowBorderTop");
            _borderTopRight = (FrameworkElement)GetTemplateChild("PART_WindowBorderTopRight");
            _borderRight = (FrameworkElement)GetTemplateChild("PART_WindowBorderRight");
            _borderBottomRight = (FrameworkElement)GetTemplateChild("PART_WindowBorderBottomRight");
            _borderBottom = (FrameworkElement)GetTemplateChild("PART_WindowBorderBottom");
            _borderBottomLeft = (FrameworkElement)GetTemplateChild("PART_WindowBorderBottomLeft");

            registerBorderEvents(WindowBorderEdge.Left, _borderLeft);
            registerBorderEvents(WindowBorderEdge.TopLeft, _borderTopLeft);
            registerBorderEvents(WindowBorderEdge.Top, BorderTop);
            registerBorderEvents(WindowBorderEdge.TopRight, _borderTopRight);
            registerBorderEvents(WindowBorderEdge.Right, _borderRight);
            registerBorderEvents(WindowBorderEdge.BottomRight, _borderBottomRight);
            registerBorderEvents(WindowBorderEdge.Bottom, _borderBottom);
            registerBorderEvents(WindowBorderEdge.BottomLeft, _borderBottomLeft);
        }

        private void registerBorderEvents(WindowBorderEdge borderEdge, FrameworkElement border)
        {
            border.MouseEnter += (sender, e) =>
            {
                if (WindowState != WindowState.Maximized && ResizeMode == ResizeMode.CanResize)
                {
                    switch (borderEdge)
                    {
                        case WindowBorderEdge.Left:
                        case WindowBorderEdge.Right:
                            border.Cursor = Cursors.SizeWE;
                            break;
                        case WindowBorderEdge.TopLeft:
                        case WindowBorderEdge.BottomRight:
                            border.Cursor = Cursors.SizeNWSE;
                            break;
                        case WindowBorderEdge.Top:
                        case WindowBorderEdge.Bottom:
                            border.Cursor = Cursors.SizeNS;
                            break;
                        case WindowBorderEdge.TopRight:
                        case WindowBorderEdge.BottomLeft:
                            border.Cursor = Cursors.SizeNESW;
                            break;
                    }
                }
                else
                {
                    border.Cursor = Cursors.Arrow;
                }
            };

            border.MouseLeftButtonDown += (sender, e) =>
            {
                if (WindowState != WindowState.Maximized && ResizeMode == ResizeMode.CanResize)
                {
                    var cursorLocation = e.GetPosition(this);
                    _cursorOffset = new Point();

                    switch (borderEdge)
                    {
                        case WindowBorderEdge.Left:
                            _cursorOffset.X = cursorLocation.X;
                            break;
                        case WindowBorderEdge.TopLeft:
                            _cursorOffset.X = cursorLocation.X;
                            _cursorOffset.Y = cursorLocation.Y;
                            break;
                        case WindowBorderEdge.Top:
                            _cursorOffset.Y = cursorLocation.Y;
                            break;
                        case WindowBorderEdge.TopRight:
                            _cursorOffset.X = Width - cursorLocation.X;
                            _cursorOffset.Y = cursorLocation.Y;
                            break;
                        case WindowBorderEdge.Right:
                            _cursorOffset.X = Width - cursorLocation.X;
                            break;
                        case WindowBorderEdge.BottomRight:
                            _cursorOffset.X = Width - cursorLocation.X;
                            _cursorOffset.Y = Height - cursorLocation.Y;
                            break;
                        case WindowBorderEdge.Bottom:
                            _cursorOffset.Y = Height - cursorLocation.Y;
                            break;
                        case WindowBorderEdge.BottomLeft:
                            _cursorOffset.X = cursorLocation.X;
                            _cursorOffset.Y = Height - cursorLocation.Y;
                            break;
                    }

                    border.CaptureMouse();
                }
            };

            border.MouseMove += (sender, e) =>
            {
                if (WindowState != WindowState.Maximized && border.IsMouseCaptured && ResizeMode == ResizeMode.CanResize)
                {
                    var cursorLocation = e.GetPosition(this);

                    var nHorizontalChange = (cursorLocation.X - _cursorOffset.X);
                    var pHorizontalChange = (cursorLocation.X + _cursorOffset.X);
                    var nVerticalChange = (cursorLocation.Y - _cursorOffset.Y);
                    var pVerticalChange = (cursorLocation.Y + _cursorOffset.Y);

                    switch (borderEdge)
                    {
                        case WindowBorderEdge.Left:
                            if (Width - nHorizontalChange <= MinWidth)
                                break;
                            Left += nHorizontalChange;
                            Width -= nHorizontalChange;
                            break;
                        case WindowBorderEdge.TopLeft:
                            if (Width - nHorizontalChange <= MinWidth)
                                break;
                            Left += nHorizontalChange;
                            Width -= nHorizontalChange;
                            if (Height - nVerticalChange <= MinHeight)
                                break;
                            Top += nVerticalChange;
                            Height -= nVerticalChange;
                            break;
                        case WindowBorderEdge.Top:
                            if (Height - nVerticalChange <= MinHeight)
                                break;
                            Top += nVerticalChange;
                            Height -= nVerticalChange;
                            break;
                        case WindowBorderEdge.TopRight:
                            if (pHorizontalChange <= MinWidth)
                                break;
                            Width = pHorizontalChange;
                            if (Height - nVerticalChange <= MinHeight)
                                break;
                            Top += nVerticalChange;
                            Height -= nVerticalChange;
                            break;
                        case WindowBorderEdge.Right:
                            if (pHorizontalChange <= MinWidth)
                                break;
                            Width = pHorizontalChange;
                            break;
                        case WindowBorderEdge.BottomRight:
                            if (pHorizontalChange <= MinWidth)
                                break;
                            Width = pHorizontalChange;
                            if (pVerticalChange <= MinHeight)
                                break;
                            Height = pVerticalChange;
                            break;
                        case WindowBorderEdge.Bottom:
                            if (pVerticalChange <= MinHeight)
                                break;
                            Height = pVerticalChange;
                            break;
                        case WindowBorderEdge.BottomLeft:
                            if (Width - nHorizontalChange <= MinWidth)
                                break;
                            Left += nHorizontalChange;
                            Width -= nHorizontalChange;
                            if (pVerticalChange <= MinHeight)
                                break;
                            Height = pVerticalChange;
                            break;
                    }
                }
            };

            border.MouseLeftButtonUp += (sender, args) => border.ReleaseMouseCapture();
        }

        private void registerFrame()
        {
            _frame = (FrameworkElement)GetTemplateChild("PART_WindowFrame");
        }

        private void registerCloseButton()
        {
            _closeButton = (Image)GetTemplateChild("PART_WindowCaptionCloseButton");

            _closeButtonOn = new BitmapImage(new Uri("img/close_on.png", UriKind.Relative));
            _closeButtonOff = new BitmapImage(new Uri("img/close_off.png", UriKind.Relative));

            if (_closeButton != null)
            {
                _closeButton.MouseLeftButtonDown += (sender, args) =>
                {
                    try
                    {
                        Close();
                    }
                    catch (InvalidOperationException)
                    {
                        Application.Current.Shutdown();
                    }
                };
                _closeButton.MouseEnter += (sender, e) => _closeButton.Source = _closeButtonOn;
                _closeButton.MouseLeave += (sender, e) => _closeButton.Source = _closeButtonOff;
            }
        }

        private void registerMaximizeButton()
        {
            _maximizeButton = (Image)GetTemplateChild("PART_WindowCaptionMaximizeButton");

            _maxButtonOn = new BitmapImage(new Uri("img/max_on.png", UriKind.Relative));
            _maxButtonOff = new BitmapImage(new Uri("img/max_off.png", UriKind.Relative));

            if (_maximizeButton != null)
            {
                _maximizeButton.MouseLeftButtonDown += (sender, e) =>
                {
                    if (WindowState == WindowState.Normal)
                    {
                        _frame.Margin = new Thickness(0);
                        WindowState = WindowState.Maximized;
                    }
                    else
                    {
                        WindowState = WindowState.Normal;
                        _frame.Margin = new Thickness(20);
                    }
                };

                _maximizeButton.MouseEnter += (sender, args) => _maximizeButton.Source = _maxButtonOn;
                _maximizeButton.MouseLeave += (sender, args) => _maximizeButton.Source = _maxButtonOff;
            }
        }

        private void registerMinimizeButton()
        {
            _minimizeButton = (Image)GetTemplateChild("PART_WindowCaptionMinimizeButton");

            _minButtonOn = new BitmapImage(new Uri("img/min_on.png", UriKind.Relative));
            _minButtonOff = new BitmapImage(new Uri("img/min_off.png", UriKind.Relative));

            if (_minimizeButton != null)
            {
                _minimizeButton.MouseLeftButtonDown += (sender, e) => WindowState = WindowState.Minimized;
                _minimizeButton.MouseEnter += (sender, args) => _minimizeButton.Source = _minButtonOn;
                _minimizeButton.MouseLeave += (sender, args) => _minimizeButton.Source = _minButtonOff;
            }
        }

        private void registerCaption()
        {
            _caption = (FrameworkElement)GetTemplateChild("PART_WindowCaption");

            if (_caption != null)
            {
                _caption.MouseLeftButtonDown += (sender, e) =>
                {
                    _restoreTop = e.GetPosition(this).Y;

                    if (e.ClickCount == 2 && e.ChangedButton == MouseButton.Left &&
                        (ResizeMode != ResizeMode.CanMinimize && ResizeMode != ResizeMode.NoResize))
                    {
                        if (WindowState != WindowState.Maximized)
                        {
                            _frame.Margin = new Thickness(0);
                            WindowState = WindowState.Maximized;
                        }
                        else
                        {
                            WindowState = WindowState.Normal;
                            _frame.Margin = new Thickness(20);
                        }

                        return;
                    }

                    DragMove();
                };

                _caption.MouseMove += (sender, e) =>
                {
                    if (e.LeftButton == MouseButtonState.Pressed && _caption.IsMouseOver &&
                        WindowState == WindowState.Maximized)
                    {
                        WindowState = WindowState.Normal;
                        Top = _restoreTop - 10;
                        DragMove();
                    }
                };
            }
        }

        private void wmGetMinMaxInfo(IntPtr hwnd, IntPtr lParam)
        {
            var mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // ReSharper disable once InconsistentNaming
            const int MONITOR_DEFAULTTONEAREST = 0x00000002;
            var monitor = NativeMethods.MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != IntPtr.Zero)
            {
                var monitorInfo = new MONITORINFO();
                NativeMethods.GetMonitorInfo(monitor, monitorInfo);

                var rcWorkArea = monitorInfo.rcWork;
                var rcMonitorArea = monitorInfo.rcMonitor;

                mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        private IntPtr wndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case NativeMethods.Messages.GetMinMax:
                    wmGetMinMaxInfo(hwnd, lParam);
                    handled = true;
                    break;
                case NativeMethods.Messages.ChangeCbChain:
                    if (wParam == _nextClipboardViewer)
                        _nextClipboardViewer = lParam;
                    else
                        NativeMethods.SendMessage(_nextClipboardViewer, msg, wParam, lParam);
                    break;
                case NativeMethods.Messages.DrawClipboard:
                    if (Clipboard.ContainsText())
                        OnClipboardTextChanged();

                    NativeMethods.SendMessage(_nextClipboardViewer, msg, wParam, lParam);
                    break;
            }

            return IntPtr.Zero;
        }
        private bool isValid(DependencyObject obj)
        {
            // The dependency object is valid if it has no errors and all
            // of its children (that are dependency objects) are error-free.
            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(isValid);
        }

        protected virtual void OnClipboardTextChanged()
        {
            if (clipboardTextChanged != null)
            {
                clipboardTextChanged(this, new EventArgs());
            }
        }

        protected string ChooseFile(string defaultExtension, string filter)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = defaultExtension,
                Filter = filter
            };

            return dlg.ShowDialog() == true ? dlg.FileName : null;
        }

        private enum WindowBorderEdge
        {
            Left,
            TopLeft,
            Top,
            TopRight,
            Right,
            BottomRight,
            Bottom,
            BottomLeft
        }
    }
}