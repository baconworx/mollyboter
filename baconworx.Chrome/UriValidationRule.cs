﻿using System;
using System.Windows.Controls;

namespace baconworx.Chrome
{
    public class UriValidationRule : ValidationRule
    {

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var textEntered = (string) value;
            if(string.IsNullOrEmpty(textEntered))
                return new ValidationResult(true, null);

            Uri uriOutput;

            return !Uri.TryCreate(textEntered, UriKind.Absolute, out uriOutput)
                ? new ValidationResult(false, "Please enter a valid Uri / File path.")
                : new ValidationResult(true, null);
        }
    }
}
