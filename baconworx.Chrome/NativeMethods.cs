﻿using System;
using System.Runtime.InteropServices;

namespace fasm.Chrome
{
    internal class NativeMethods
    {
        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        [DllImport("user32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr hwnd, int flags);

        [DllImport("user32")]
        internal static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

        [DllImport("user32")]
        internal static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        [DllImport("user32")]
        internal static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Windows Event Messages sent to the WindowProc
        /// </summary>
        internal static class Messages
        {
            public const int GetMinMax = 0x024;
            public const int DrawClipboard = 0x308;
            public const int ChangeCbChain = 0x030D;
        }
    }
}