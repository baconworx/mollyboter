﻿using System;
using System.Windows.Markup;
using System.Xaml;

namespace fasm.Chrome
{

    public class RootExtension : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            IRootObjectProvider provider = serviceProvider.GetService
            (typeof(IRootObjectProvider)) as IRootObjectProvider;
            if (provider != null)
            {
                return provider.RootObject;
            }

            return null;
        }
    }    
}