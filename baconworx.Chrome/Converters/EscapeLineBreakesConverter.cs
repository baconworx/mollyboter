﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace baconworx.Chrome.Converters
{
    public class EscapeLineBreakesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (string)value;
            return string.IsNullOrEmpty(s) ? s : s.Replace("\n", @"\n");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (string)value;
            return string.IsNullOrEmpty(s) ? s : s.Replace(@"\n", "\n");
        }
    }
}