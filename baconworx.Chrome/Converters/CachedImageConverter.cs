﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace baconworx.Chrome.Converters
{
    public class CachedImageConverter : IValueConverter
    {

        static Dictionary<string, ImageSource> cache = new Dictionary<string, ImageSource>();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string) value))
                return null;

            var filepath = (string)value;

            if (!cache.ContainsKey(filepath) && File.Exists(filepath))
                return cache[filepath] = new BitmapImage(new Uri(filepath)) {CacheOption = BitmapCacheOption.OnLoad};
            
            if (!cache.ContainsKey(filepath))
                return cache[filepath] = null;

            return cache[filepath];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}