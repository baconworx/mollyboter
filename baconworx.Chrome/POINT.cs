using System.Runtime.InteropServices;

namespace fasm.Chrome
{
    [StructLayout(LayoutKind.Sequential)]
    // ReSharper disable once InconsistentNaming
    public struct POINT
    {
        public int x;
        public int y;
    }
}