using System.Runtime.InteropServices;

namespace fasm.Chrome
{
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    // ReSharper disable once InconsistentNaming
    public struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }
}