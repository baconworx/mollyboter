using System.Runtime.InteropServices;

namespace fasm.Chrome
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    // ReSharper disable once InconsistentNaming
    public class MONITORINFO
    {
        public int cbSize = Marshal.SizeOf(typeof(MINMAXINFO));
        public RECT rcMonitor = new RECT();
        public RECT rcWork = new RECT();
        public int dwFlags = 0;
    }
}