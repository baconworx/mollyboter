﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace baconworx.Chrome
{
    public abstract class AsyncValidationAttribute : Attribute
    {
        public string ErrorMessage { get; set; }
        public abstract Task<bool> IsValid(object value, ValidationContext validationContext);
    }
}
