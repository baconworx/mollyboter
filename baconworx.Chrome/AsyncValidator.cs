﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace baconworx.Chrome
{
    public class AsyncValidator
    {
        public static void TryValidateProperty(object value, ValidationContext validationContext, Action<ValidationResult> completedAction)
        {
            var property = validationContext.ObjectInstance.GetType().GetProperty(validationContext.MemberName);

            foreach (var attribute in property.GetCustomAttributes(typeof(AsyncValidationAttribute), true).Select(a => (AsyncValidationAttribute)a))
                Task.Run(async () =>
                {
                    var errorMessage = string.Empty;
                    if (!await attribute.IsValid(value, validationContext))
                        errorMessage = attribute.ErrorMessage;

                    completedAction(new ValidationResult(errorMessage, new List<string>(1) { property.Name }));
                });
        }
        public static void TryValidateObject(ValidationModel viewModel, ValidationContext validationContext, Action<ValidationResult> completedAction)
        {
            var properties = viewModel.GetType().GetProperties().Where(prop => prop.GetCustomAttributes(typeof(AsyncValidationAttribute), true).Any());

            foreach (var property in properties)
                foreach (var attribute in property.GetCustomAttributes(typeof (AsyncValidationAttribute), true).Select(a => (AsyncValidationAttribute) a))
                    Task.Run(async () =>
                    {
                        var errorMessage = string.Empty;
                        if (!await attribute.IsValid(property.GetValue(viewModel), validationContext).ConfigureAwait(false))
                            errorMessage = attribute.ErrorMessage;

                        completedAction(new ValidationResult(errorMessage, new List<string>(1) {property.Name}));
                    });
        }
    }
}
