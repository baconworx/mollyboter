﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Threading.Tasks;
using baconworx.Chrome;

namespace baconworx.Mollyboter.ObsPlugin.ValidationAttributes
{
    internal class IsValidTwitchChannelAttribute : AsyncValidationAttribute, IDisposable
    {
        private const string TWITCH_CHECK_USERNAME_ENDPOINT = "https://api.twitch.tv/kraken/channels/{0}";
        private const string ERROR_MESSAGE_TEMPLATE = "Channel '{0}' does not exist";
        private static readonly Lazy<HttpClient> LazyHttpClient = new Lazy<HttpClient>(() => new HttpClient());
        private static readonly Dictionary<string, bool> _cache = new Dictionary<string, bool>();

        public override async Task<bool> IsValid(object value, ValidationContext validationContext)
        {
            var username = value as string;
            if (username == null) return false;

            if (_cache.ContainsKey(username)) return _cache[username];

            try
            {
                if (LazyHttpClient.IsValueCreated)
                    LazyHttpClient.Value.CancelPendingRequests();

                await LazyHttpClient.Value.GetStringAsync(string.Format(TWITCH_CHECK_USERNAME_ENDPOINT, username.Replace(" ", "%20"))).ConfigureAwait(false);
            }
            catch (Exception ignored)
            {
                /* 404 not found, probably.. */
                ErrorMessage = string.Format(ERROR_MESSAGE_TEMPLATE, username);
                return _cache[username] = false;
            }

            return _cache[username] = true;
        }

        public void Dispose()
        {
            if (LazyHttpClient.IsValueCreated)
                LazyHttpClient.Value.Dispose();
        }
    }
}
