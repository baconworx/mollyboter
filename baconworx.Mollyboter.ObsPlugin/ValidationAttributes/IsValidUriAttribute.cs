﻿using System;
using System.ComponentModel.DataAnnotations;

namespace baconworx.Mollyboter.ObsPlugin.ValidationAttributes
{
    internal class IsValidUriAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var enteredText = (string) value;
            Uri uri;
            return new ValidationResult(string.IsNullOrEmpty(enteredText) || Uri.TryCreate(enteredText, UriKind.Absolute, out uri) ? string.Empty : ErrorMessage);
        }
    }
}
