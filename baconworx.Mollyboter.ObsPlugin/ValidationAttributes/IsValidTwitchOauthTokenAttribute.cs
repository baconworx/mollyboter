﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using baconworx.Chrome;
using Newtonsoft.Json.Linq;

namespace baconworx.Mollyboter.ObsPlugin.ValidationAttributes
{
    internal class IsValidTwitchOauthTokenAttribute : AsyncValidationAttribute
    {
        private const string TWITCH_CHECK_TOKEN_ENDPOINT = "https://api.twitch.tv/kraken?oauth_token={0}";
        private static readonly Lazy<HttpClient> LazyHttpClient = new Lazy<HttpClient>(() => new HttpClient());

        private static readonly Dictionary<string, Dictionary<string, string>> _cache = new Dictionary<string, Dictionary<string, string>>();

        public string[] Scopes { get; set; }

        public string CheckChannelPropertyName { get; set; } = null;

        private const string ERROR_MSG_INVALID_CHANNEL = "Invalid for this Channel!";
        private const string ERROR_MSG_INVALID_SCOPES = "Missing required scopes!";
        private const string ERROR_MSG_INVALID_TOKEN = "Invalid Oauth-Token!";
        private const string ERROR_MSG_VALID = "";

        public override async Task<bool> IsValid(object value, ValidationContext validationContext)
        {
            var oauthToken = value as string;
            if (oauthToken == null) return false;

            try
            {
                var channelProperty = validationContext.ObjectType.GetProperty(CheckChannelPropertyName);
                var channel = (string)channelProperty.GetValue(validationContext.ObjectInstance);
                if (_cache.ContainsKey(oauthToken) && _cache[oauthToken].ContainsKey(channel))
                {
                    if (_cache[oauthToken][channel] == ERROR_MSG_VALID)
                        return true;

                    ErrorMessage = _cache[oauthToken][channel];
                    return false;
                }

                if (LazyHttpClient.IsValueCreated)
                    LazyHttpClient.Value.CancelPendingRequests();

                var response = await LazyHttpClient.Value.GetStringAsync(string.Format(TWITCH_CHECK_TOKEN_ENDPOINT, oauthToken.Substring(6))).ConfigureAwait(false);
                var jsonResponse = JObject.Parse(response);
                if ((bool)jsonResponse["token"]["valid"])
                {
                    if (Scopes.All(scope => jsonResponse["token"]["authorization"]["scopes"].ToList().Contains(scope)))
                    {
                        if (CheckChannelPropertyName == null) return true;

                        if ((string)jsonResponse["token"]["user_name"] == channel)
                        {
                            addErrorToCache(oauthToken, channel, string.Empty);
                            return true;
                        }

                        addErrorToCache(oauthToken, channel, ErrorMessage = ERROR_MSG_INVALID_CHANNEL);
                        return false;
                    }
                    addErrorToCache(oauthToken, channel, ErrorMessage = ERROR_MSG_INVALID_SCOPES);
                    return false;
                }

                addErrorToCache(oauthToken, channel, ErrorMessage = ERROR_MSG_INVALID_TOKEN);
            }
            catch (Exception ignored)
            {
                /* whatever.. */
                ErrorMessage = "Internal error!";
                return false;
            }

            return false;
        }

        private void addErrorToCache(string oauthToken, string channel, string errorMessage)
        {
            if(!_cache.ContainsKey(oauthToken))
                _cache[oauthToken] = new Dictionary<string, string>();

            _cache[oauthToken][channel] = errorMessage;
        }
    }
}
