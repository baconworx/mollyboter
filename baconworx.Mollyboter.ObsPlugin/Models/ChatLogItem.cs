﻿using System;
using System.Web;
using System.Windows.Input;
using Newtonsoft.Json;

namespace baconworx.Mollyboter.ObsPlugin.Models
{
    public class ChatLogItem
    {
        public DateTime DateTime { get; set; }
        public MessageDirection Direction { get; set; }
        public string Content { get; set; }

        private const string REGEX_TOOL_WEBSITE = "http://regexstorm.net/tester?i={0}";


        public ChatLogItem(MessageDirection direction, string content, DateTime? dateTime = null)
        {
            DateTime = dateTime ?? DateTime.UtcNow;
            Direction = direction;
            Content = content;

            OpenRegexToolCommand = new OpenWebsiteCommand(REGEX_TOOL_WEBSITE);
        }

        [JsonIgnore]
        public OpenWebsiteCommand OpenRegexToolCommand { get; internal set; }

        public class OpenWebsiteCommand : ICommand
        {
            public string EndpointFormatString { get; set; }

            public OpenWebsiteCommand(string endpointFormatString)
            {
                EndpointFormatString = endpointFormatString;
            }


            public event EventHandler CanExecuteChanged;

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public void Execute(object parameter)
            {
                var item = (ChatLogItem)parameter;
                System.Diagnostics.Process.Start(string.Format(EndpointFormatString, Uri.EscapeDataString(item.Content)));
            }

            protected virtual void OnCanExecuteChanged(EventArgs e) => CanExecuteChanged?.Invoke(this, e);
        }
    }
}
