﻿using System;

namespace baconworx.Mollyboter.ObsPlugin.Models
{
    public class Notification
    {
        public string DisplayText { get; set; }
        public Uri AudioUri { get; set; }
        public bool PlayAudioToEnd { get; set; }

        public Notification(string displayText, Uri audioUri = null, bool playAudioToEnd = false)
        {
            DisplayText = displayText;
            AudioUri = audioUri;
            PlayAudioToEnd = playAudioToEnd;
        }
    }
}
