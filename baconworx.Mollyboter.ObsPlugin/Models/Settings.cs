﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Color = System.Windows.Media.Color;

namespace baconworx.Mollyboter.ObsPlugin.Models
{
    public class Settings : ICloneable
    {
        public string OauthToken { get; set; }
        public string Channel { get; set; }
        public string ImageFile { get; set; }
        public int Fps { get; set; }
        public Rectangle TextRect { get; set; }
        public int FontSize { get; set; }
        public Color TextColor { get; set; }
        public Color TextBackground { get; set; }
        public string FontFile { get; set; }
        public TimeSpan UserCooldown { get; set; }
        public List<DisplayTrigger> DisplayTriggers { get; set; } = new List<DisplayTrigger>();
        public Settings Clone()
        {
            return new Settings
            {
                Channel = Channel,
                OauthToken = OauthToken,
                ImageFile = ImageFile,
                Fps = Fps,
                TextRect = TextRect,
                FontSize = FontSize,
                TextColor = TextColor,
                TextBackground = TextBackground,
                FontFile = FontFile,
                UserCooldown = UserCooldown,
                DisplayTriggers = DisplayTriggers.ConvertAll(item => item.Clone())
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}