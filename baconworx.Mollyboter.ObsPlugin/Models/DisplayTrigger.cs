﻿using System;

namespace baconworx.Mollyboter.ObsPlugin.Models
{
    public class DisplayTrigger : ICloneable
    {
        public string Name { get; set; }
        public string Regex { get; set; }
        public string DisplayPattern { get; set; }
        public Uri AudioUri { get; set; }
        public bool PlayAudioToEnd { get; set; }
        public bool CaseSensitive { get; set; }

        public DisplayTrigger Clone()
        {
            return new DisplayTrigger
            {
                Name = Name,
                Regex = Regex,
                DisplayPattern = DisplayPattern,
                AudioUri = AudioUri,
                PlayAudioToEnd = PlayAudioToEnd,
                CaseSensitive = CaseSensitive
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}