﻿using System;
using System.ComponentModel.DataAnnotations;
using baconworx.Chrome;
using baconworx.Mollyboter.ObsPlugin.Models;
using baconworx.Mollyboter.ObsPlugin.ValidationAttributes;

namespace baconworx.Mollyboter.ObsPlugin.ViewModels
{
    class DisplayTriggerViewModel : ValidationModel
    {
        private readonly DisplayTrigger _displayTrigger;
        public DisplayTriggerViewModel(DisplayTrigger displayTrigger)
        {
            _displayTrigger = displayTrigger;

            SetValue(() => Name, _displayTrigger.Name);
            SetValue(() => Regex, _displayTrigger.Regex);
            SetValue(() => DisplayPattern, _displayTrigger.DisplayPattern);
            SetValue(() => AudioUri, _displayTrigger.AudioUri);
            SetValue(() => PlayAudioToEnd, _displayTrigger.PlayAudioToEnd);
            SetValue(() => CaseSensitive, _displayTrigger.CaseSensitive);
        }

        public DisplayTrigger Model => _displayTrigger;
        
        [Required(ErrorMessage = "Please name your rule.")]
        public string Name
        {
            get { return GetValue(() => Name); }
            set { SetValue(() => Name, _displayTrigger.Name = value); }
        }

        [Required(ErrorMessage = "Please provide a catch-phrase in Regex.")]
        public string Regex
        {
            get { return GetValue(() => Regex); }
            set { SetValue(() => Regex, _displayTrigger.Regex = value); }
        }

        [Required(ErrorMessage = "Please provide a display pattern.")]
        public string DisplayPattern
        {
            get { return GetValue(() => DisplayPattern); }
            set { SetValue(() => DisplayPattern, _displayTrigger.DisplayPattern = value); }
        }
        
        public Uri AudioUri
        {
            get { return GetValue(() => AudioUri); }
            set { SetValue(() => AudioUri, _displayTrigger.AudioUri = value); }
        }

        public bool PlayAudioToEnd
        {
            get { return GetValue(() => PlayAudioToEnd); }
            set { SetValue(() => PlayAudioToEnd, _displayTrigger.PlayAudioToEnd = value); }
        }
        public bool CaseSensitive
        {
            get { return GetValue(() => CaseSensitive); }
            set { SetValue(() => CaseSensitive, _displayTrigger.CaseSensitive = value); }
        }

    }
}
