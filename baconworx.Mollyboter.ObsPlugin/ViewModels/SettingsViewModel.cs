﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using baconworx.Chrome;
using baconworx.Mollyboter.ObsPlugin.Models;
using baconworx.Mollyboter.ObsPlugin.ValidationAttributes;
using Color = System.Windows.Media.Color;

namespace baconworx.Mollyboter.ObsPlugin.ViewModels
{
    class SettingsViewModel : ValidationModel
    {
        private readonly Settings _settings;
        private List<DisplayTriggerViewModel> _displayTriggers;
        public SettingsViewModel(Settings settings)
        {
            _settings = settings;
            _displayTriggers = settings.DisplayTriggers.ConvertAll(trigger => new DisplayTriggerViewModel(trigger));

            SetValue(() => OauthToken, _settings.OauthToken ?? string.Empty);
            SetValue(() => Channel, _settings.Channel ?? string.Empty);
            SetValue(() => ImageFile, _settings.ImageFile ?? string.Empty);
            SetValue(() => TextRectX, _settings.TextRect.X.ToString("0"));
            SetValue(() => TextRectY, _settings.TextRect.Y.ToString("0"));
            SetValue(() => TextRectWidth, _settings.TextRect.Width.ToString("0"));
            SetValue(() => TextRectHeight, _settings.TextRect.Height.ToString("0"));
            SetValue(() => FontFile, _settings.FontFile ?? string.Empty);
            SetValue(() => FontSize, _settings.FontSize);
            SetValue(() => Fps, _settings.Fps);
            SetValue(() => TextColor, _settings.TextColor);
            SetValue(() => TextBackground, _settings.TextBackground);
            SetValue(() => UserCooldown, _settings.UserCooldown);
            SetValue(() => DisplayTriggers, _displayTriggers);
        }
        public Settings Model => _settings;

        [Required(ErrorMessage = "What's your Twitch channel's name?")]
        [IsValidTwitchChannel]
        public string Channel
        {
            get { return GetValue(() => Channel); }
            set { SetValue(() => Channel, _settings.Channel = value); }
        }


        [Required(ErrorMessage = "Please enter an Oauth-Token!")]
        [IsValidTwitchOauthToken(Scopes = new[] { "channel_check_subscription", "chat_login" }, CheckChannelPropertyName = nameof(Channel))]
        public string OauthToken
        {
            get { return GetValue(() => OauthToken); }
            set { SetValue(() => OauthToken, _settings.OauthToken = value); }
        }

        [Required(ErrorMessage = "Please provide an overlay image!")]
        public string ImageFile
        {
            get { return GetValue(() => ImageFile); }
            set { SetValue(() => ImageFile, _settings.ImageFile = value); }
        }

        public int Fps
        {
            get { return GetValue(() => Fps); }
            set { SetValue(() => Fps, _settings.Fps = value); }
        }
        public string TextRectX
        {
            get { return GetValue(() => TextRectX); }
            set
            {
                int intVal;
                if (int.TryParse(value, out intVal))
                    _settings.TextRect = new Rectangle(new Point(intVal, _settings.TextRect.Y), _settings.TextRect.Size);

                SetValue(() => TextRectX, value);
            }
        }
        public string TextRectY
        {
            get { return GetValue(() => TextRectY); }
            set
            {
                int intVal;
                if (int.TryParse(value, out intVal))
                    _settings.TextRect = new Rectangle(new Point(_settings.TextRect.X, intVal), _settings.TextRect.Size);

                SetValue(() => TextRectY, value);
            }
        }
        public string TextRectWidth
        {
            get { return GetValue(() => TextRectWidth); }
            set
            {
                int intVal;
                if (int.TryParse(value, out intVal))
                    _settings.TextRect = new Rectangle(_settings.TextRect.Location, new Size(intVal, _settings.TextRect.Height));
                SetValue(() => TextRectWidth, value);
            }
        }
        public string TextRectHeight
        {
            get { return GetValue(() => TextRectHeight); }
            set
            {
                int intVal;
                if (int.TryParse(value, out intVal))
                    _settings.TextRect = new Rectangle(_settings.TextRect.Location, new Size(_settings.TextRect.Width, intVal));
                SetValue(() => TextRectHeight, value);
            }
        }
        public int FontSize
        {
            get { return GetValue(() => FontSize); }
            set { SetValue(() => FontSize, _settings.FontSize = value); }
        }
        public Color TextColor
        {
            get { return GetValue(() => TextColor); }
            set { SetValue(() => TextColor, _settings.TextColor = value); }
        }
        public Color TextBackground
        {
            get { return GetValue(() => TextBackground); }
            set { SetValue(() => TextBackground, _settings.TextBackground = value); }
        }
        [Required(ErrorMessage = "Please provide a font file!")]
        public string FontFile
        {
            get { return GetValue(() => FontFile); }
            set { SetValue(() => FontFile, _settings.FontFile = value); }
        }
        public TimeSpan UserCooldown
        {
            get { return GetValue(() => UserCooldown); }
            set { SetValue(() => UserCooldown, _settings.UserCooldown = value); }
        }
        public List<DisplayTriggerViewModel> DisplayTriggers
        {
            get { return GetValue(() => DisplayTriggers); }
            set { SetValue(() => DisplayTriggers, _displayTriggers = value); }
        }


        public DisplayTriggerViewModel AddTrigger()
        {
            var trigger = new DisplayTrigger();
            _settings.DisplayTriggers.Add(trigger);
            var viewModelTrigger = new DisplayTriggerViewModel(trigger);
            DisplayTriggers.Add(viewModelTrigger);
            return viewModelTrigger;
        }
        public void RemoveTrigger(DisplayTriggerViewModel displayTrigger)
        {
            DisplayTriggers.Remove(displayTrigger);
            _settings.DisplayTriggers.Remove(displayTrigger.Model);
            _displayTriggers.Remove(displayTrigger);
        }

    }
}
