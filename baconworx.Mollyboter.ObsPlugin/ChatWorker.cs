﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using baconworx.Mollyboter.ObsPlugin.Models;
using Newtonsoft.Json.Linq;

namespace baconworx.Mollyboter.ObsPlugin
{
    internal class ChatWorker
    {
        private const string TWITCH_API_CHATPROP_ENDPOINT = "https://api.twitch.tv/api/channels/{0}/chat_properties";

        private const string TWITCH_API_SUBCHECK_ENDPOINT =
            "https://api.twitch.tv/kraken/channels/{0}/subscriptions/{1}";

        private readonly Settings _settings;
        private readonly ObservableFixedSizedQueue<ChatLogItem> _chatLog;

        private readonly Regex _usernameRegex =
            new Regex(@"^.*:(?<username>[^!]+)![^@]+@[^\.]+\.[^\.]+\.twitch\.tv PRIVMSG #.+?:.*$");

        private TcpClient _client;
        private bool _supressMessages;

        public ChatWorker(Settings settings, ObservableFixedSizedQueue<ChatLogItem> chatLog)
        {
            _settings = settings;
            _chatLog = chatLog;
        }

        public void Run(Action<string, Notification> customAction)
        {
            try
            {
                _supressMessages = false;
                string ircEndpointHost;
                int ircPort;

                using (var httpClient = new HttpClient())
                {
                    var httpTask =
                        httpClient.GetStringAsync(string.Format(TWITCH_API_CHATPROP_ENDPOINT, _settings.Channel));
                    Task.WaitAll(new Task[] { httpTask }, TimeSpan.FromSeconds(30));
                    var ircEndpoint = JObject.Parse(httpTask.Result).GetValue("chat_servers").First.Value<string>();
                    var fragments = ircEndpoint.Split(':');
                    ircEndpointHost = fragments[0];
                    ircPort = int.Parse(fragments[1]);
                }

                using (_client = new TcpClient(ircEndpointHost, ircPort))
                using (var stream = _client.GetStream())
                using (var writer = new StreamWriter(stream))
                using (var reader = new StreamReader(stream))
                {
                    sendIrc(writer, $"PASS {_settings.OauthToken}");
                    sendIrc(writer, $"NICK {_settings.Channel}");
                    sendIrc(writer, $"JOIN #{_settings.Channel}");
                    sendIrc(writer, "CAP REQ :twitch.tv/membership");
                    sendIrc(writer, "CAP REQ :twitch.tv/tags");

                    while (_client.Connected)
                        sendIrc(writer, processLine(receiveIrc(reader), customAction));
                }
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ignored) { if (!_supressMessages) MessageBox.Show("IRC Connection lost."); }
        }

        private void sendIrc(StreamWriter writer, string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            writer.Write($"{message}\n");
            writer.Flush();

            if (message.StartsWith("PASS")) message = $"PASS {new string('*', 16)}"; 
            Application.Current.Dispatcher.Invoke(() => { _chatLog.Add(new ChatLogItem(MessageDirection.Outgoing, message)); });
        }

        private string receiveIrc(StreamReader reader)
        {
            var message = reader.ReadLine() ?? string.Empty;
            Application.Current.Dispatcher.Invoke(() => { _chatLog.Add(new ChatLogItem(MessageDirection.Incoming, message)); });

            return message;
        }

        public void Stop()
        {
            _supressMessages = true;
            try
            {
                if (_client.Connected)
                    _client.Close();
            }
            catch { /* ignored */ }
        }

        private string processLine(string chatLine, Action<string, Notification> customAction)
        {
            if (chatLine.StartsWith("PING "))
                return "PONG tmi.twitch.tv\n";

            foreach (var displayTrigger in _settings.DisplayTriggers)
            {
                try
                {
                    var regex = new Regex(displayTrigger.Regex,
                        displayTrigger.CaseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase);
                    var match = regex.Match(chatLine);

                    if (!match.Success) continue;

                    var displayString = regex.Replace(chatLine, displayTrigger.DisplayPattern);
                    var username = _usernameRegex.Match(chatLine).Groups["username"].Value;

                    customAction(username, new Notification(displayString, displayTrigger.AudioUri, displayTrigger.PlayAudioToEnd));
                }
                // ReSharper disable once UnusedVariable
                catch (Exception ignored) { /* wrong regex etc */ }
            }

            return string.Empty;
        }
    }
}