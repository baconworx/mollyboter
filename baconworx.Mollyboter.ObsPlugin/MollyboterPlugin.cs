﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using baconworx.Mollyboter.ObsPlugin.Models;
using CLROBS;
using Newtonsoft.Json;

namespace baconworx.Mollyboter.ObsPlugin
{
    public class MollyboterPlugin : Plugin
    {
        public string Description => "mollyboter chat plugin v0.8";
        public string Name => "mollyboter";


        private ChatWorker _chatWorker;
        private BlockingCollection<Notification> _notificationQueue;
        private Dictionary<string, DateTime> _cooldowns = new Dictionary<string, DateTime>();
        private Settings _settings;
        private ObservableFixedSizedQueue<ChatLogItem> _chatLog;

        internal MollyboterImageSourceFactory MollyboterImageSourceFactory { get; set; }
        public const string SETTINGS_FILENAME = @"\plugins\CLRHostPlugin\mollyboter-files\mollyboter.settings.json";
        public const string LOG_FILENAME = @"\plugins\CLRHostPlugin\mollyboter-files\mollyboter.irclog.json";
        public const int MAX_LOG_ENTRIES = 1000;

        public bool LoadPlugin()
        {
            _notificationQueue = new BlockingCollection<Notification>(new ConcurrentQueue<Notification>());

            _chatLog = _loadIrcLog();

            API.Instance.AddImageSourceFactory(MollyboterImageSourceFactory = new MollyboterImageSourceFactory(this, _notificationQueue, _chatLog));
            reloadSettings();

            return true;
        }

        private static ObservableFixedSizedQueue<ChatLogItem> _loadIrcLog()
        {
            ObservableFixedSizedQueue<ChatLogItem> ircLog;

            var filepath = $"{Directory.GetCurrentDirectory()}{LOG_FILENAME}";
            if (!File.Exists(filepath))
            {
                ircLog = new ObservableFixedSizedQueue<ChatLogItem>(MAX_LOG_ENTRIES);
                File.WriteAllText(filepath, "[]"); // write empty json list
            }
            else
                ircLog = new ObservableFixedSizedQueue<ChatLogItem>(JsonConvert.DeserializeObject<List<ChatLogItem>>(File.ReadAllText(filepath)), MAX_LOG_ENTRIES);


            ircLog.CollectionChanged += (sender, args) =>
            {
                File.WriteAllText(filepath, JsonConvert.SerializeObject(ircLog.ToList()));
            };

            return ircLog;
        }

        public void UnloadPlugin()
        {
            MollyboterImageSourceFactory.Dispose();
            _notificationQueue.Dispose();
        }

        public void OnStartStream()
        {
            reloadSettings();
            _chatWorker = new ChatWorker(_settings, _chatLog);
            Task.Run(() => _chatWorker.Run(addToDisplayQueue));
        }

        public void OnStopStream()
        {
            try { _chatWorker?.Stop(); }
            catch (Exception) { /* ignored */ }
        }

        private void reloadSettings()
        {
            try { _settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(Directory.GetCurrentDirectory() + SETTINGS_FILENAME)); }
            catch (Exception) { _settings = new Settings(); }

            MollyboterImageSourceFactory?.UpdateSettings(_settings);
        }

        private void addToDisplayQueue(string username, Notification notification)
        {
            // delete old entries
            _cooldowns = _cooldowns.Where(entry => entry.Value > DateTime.Now).ToDictionary(p => p.Key, p => p.Value);

            // user "twitchnotify" must not be sent on cooldown!
            if (username != "twitchnotify" && _cooldowns.ContainsKey(username))
                return;

            // add user to cooldown dictionary
            _cooldowns[username] = DateTime.Now.Add(_settings.UserCooldown);

            // queue display of message
            _notificationQueue.Add(notification);
        }

        public void ResetCooldowns()
        {
            _cooldowns.Clear();
        }

    }
}
