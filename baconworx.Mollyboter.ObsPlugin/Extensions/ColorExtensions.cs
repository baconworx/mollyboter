﻿using System.Windows.Media;

namespace baconworx.Mollyboter.ObsPlugin.Extensions
{
    public static class ColorExtensions
    {
        public static Color ToWpfColor(this System.Drawing.Color gdiColor)
        {
            return Color.FromArgb(gdiColor.A, gdiColor.R, gdiColor.G, gdiColor.B);
        }
        public static System.Drawing.Color ToGdiColor(this Color wpfColor)
        {
            return System.Drawing.Color.FromArgb(wpfColor.A, wpfColor.R, wpfColor.G, wpfColor.B);
        }
    }
}
