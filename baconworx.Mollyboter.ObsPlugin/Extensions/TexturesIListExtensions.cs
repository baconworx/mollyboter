﻿using System.Collections.Generic;
using CLROBS;

namespace baconworx.Mollyboter.ObsPlugin.Extensions
{
    public static class TexturesIListExtensions
    {
        public static void Dispose(this IList<Texture> collection)
        {
            foreach (var texture in collection)
                texture.Dispose();
            collection.Clear();
        }
    }
}
