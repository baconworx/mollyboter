﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using baconworx.Mollyboter.ObsPlugin.Models;

namespace baconworx.Mollyboter.ObsPlugin.ValueConverters
{
    internal class LogChatLineColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var item = value as ChatLogItem;
            if (item == null)
                return Brushes.DarkRed;

            if (item.Direction == MessageDirection.Incoming)
                return Brushes.DarkCyan;
            // outgoing:
            return Brushes.DarkOrange;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
