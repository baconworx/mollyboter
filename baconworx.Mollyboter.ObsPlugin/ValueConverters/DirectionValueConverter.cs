﻿using System;
using System.Globalization;
using System.Windows.Data;
using baconworx.Mollyboter.ObsPlugin.Models;

namespace baconworx.Mollyboter.ObsPlugin.ValueConverters
{
    internal class DirectionValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var direction = (MessageDirection)value;
            return direction == MessageDirection.Incoming ? "<" : ">";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
