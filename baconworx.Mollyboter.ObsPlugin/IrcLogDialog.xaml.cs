﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using baconworx.Mollyboter.ObsPlugin.Models;

namespace baconworx.Mollyboter.ObsPlugin
{
    public partial class IrcLogDialog
    {

        public IrcLogDialog(ObservableFixedSizedQueue<ChatLogItem> chatLog)
        {
            DataContext = chatLog;

            InitializeComponent();

            var logListView = (ListView)FindName("LogListView");

            Loaded += (sernder, args) =>
            {
                var border = (Decorator)VisualTreeHelper.GetChild(logListView, 0);
                var scrollViewer = (ScrollViewer)border.Child;
                scrollViewer.ScrollToEnd();

                chatLog.CollectionChanged += (innerSender, innerArgs) =>
                {
                    if (Math.Abs(scrollViewer.VerticalOffset - scrollViewer.ScrollableHeight) < 1)
                        scrollViewer.ScrollToEnd();
                };
            };
        }
    }
}
