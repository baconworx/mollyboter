﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace baconworx.Mollyboter.ObsPlugin
{
    public class ObservableFixedSizedQueue<T> : ObservableCollection<T>
    {
        public int Limit { get; set; }

        public ObservableFixedSizedQueue(int limit)
        {
            Limit = limit;
        }

        public ObservableFixedSizedQueue(IEnumerable<T> collection, int limit)
            : base(collection)
        {
            Limit = limit;
        }

        protected override void InsertItem(int index, T item)
        {
            lock (this)
            {
                int skip;
                for (skip = 0; Count >= Limit; skip++)
                    RemoveAt(0);

                base.InsertItem(index - skip, item);
            }
        }
    }
}
