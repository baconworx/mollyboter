﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.IO;
using baconworx.Mollyboter.ObsPlugin.Models;
using CLROBS;
using Newtonsoft.Json;

namespace baconworx.Mollyboter.ObsPlugin
{
    internal class MollyboterImageSourceFactory : ImageSourceFactory, IDisposable
    {
        private readonly MollyboterPlugin _ownerPlugin;
        private readonly BlockingCollection<Notification> _notificationQueue;
        public ObservableFixedSizedQueue<ChatLogItem> ChatLog { get; private set; }
        private Settings _settings;

        internal MollyboterImageSource ImageSource { get; private set; }

        public MollyboterImageSourceFactory(MollyboterPlugin ownerPlugin, BlockingCollection<Notification> notificationQueue, ObservableFixedSizedQueue<ChatLogItem> chatLog)
        {
            _ownerPlugin = ownerPlugin;
            _notificationQueue = notificationQueue;
            ChatLog = chatLog;
        }

        public ImageSource Create(XElement data)
        {
            ImageSource = new MollyboterImageSource(_notificationQueue);
            ImageSource.UpdateSettings(_settings);
            return ImageSource;
        }

        public bool ShowConfiguration(XElement data)
        {
            var configDialog = new ConfigurationDialog(_ownerPlugin, _settings);
            if (configDialog.ShowDialog() == true)
                File.WriteAllText(Directory.GetCurrentDirectory() + MollyboterPlugin.SETTINGS_FILENAME, JsonConvert.SerializeObject(_settings, Formatting.Indented));
            
            return true;
        }

        public void UpdateSettings(Settings settings)
        {
            _settings = settings;
            ImageSource?.UpdateSettings(_settings);
        }

        public string ClassName => GetType().Name;
        public string DisplayName => "mollyboter";
        public void Dispose()
        {
            ImageSource?.Dispose();
        }
    }
}
