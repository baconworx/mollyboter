﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using baconworx.Mollyboter.ObsPlugin.Extensions;
using baconworx.Mollyboter.ObsPlugin.Models;
using CLROBS;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace baconworx.Mollyboter.ObsPlugin
{
    public class MollyboterImageSource : AbstractImageSource, IDisposable
    {
        private readonly object _textureLock = new object(), _displayLock = new object();
        private readonly List<Texture> _textures = new List<Texture>();
        private readonly BlockingCollection<Notification> _notificationQueue;
        private readonly Thread _workerThread;
        private int _currentFrame;
        private int _currentOpacity;
        private Settings _settings;
        private List<BitmapImage> _templateFrames;

        private MediaPlayer _mediaPlayer;


        public MollyboterImageSource(BlockingCollection<Notification> notificationQueue)
        {
            _notificationQueue = notificationQueue;

            _workerThread = new Thread(queueWatcherTask);
            _workerThread.Start();
        }

        public void Dispose()
        {
            _workerThread.Abort();
        }

        public override void Render(float x, float y, float width, float height)
        {
            lock (_textureLock)
            {
                if (_textures.Count > 0)
                {
                    GraphicsSystem.Instance.DrawSprite(
                        _textures[_currentFrame],
                        (uint)(0xFFFFFF + _currentOpacity * 0x1000000),
                        x, y,
                        x + Math.Min(width, _textures[_currentFrame].Width),
                        y + Math.Min(height, _textures[_currentFrame].Height));
                }
            }
        }

        public override void UpdateSettings()
        {

        }

        private void framesIteratorTask()
        {
            try
            {
                for (;;)
                {
                    _currentFrame = (_currentFrame + 1) % _templateFrames.Count;
                    Thread.Sleep(1000 / Math.Max(_settings.Fps, 1));
                }
            }
            catch (ThreadAbortException) { /* ignored */ }
        }

        private void queueWatcherTask()
        {
            _mediaPlayer = new MediaPlayer();
            try
            {
                for (;;)
                {
                    var notification = _notificationQueue.Take();

                    notify(notification);
                }
            }
            catch (ThreadAbortException) { _mediaPlayer.Stop(); }
            finally { _textures.Dispose(); }
        }

        private void notify(Notification notification, Settings settings = null)
        {
            settings = settings ?? _settings;
            lock (_displayLock)
            {
                if (notification.AudioUri != null)
                {
                    _mediaPlayer.Stop();
                    // check if audio file changed. if yes, load other media
                    if(_mediaPlayer.Source != notification.AudioUri)
                    {
                        _mediaPlayer.Close();
                        _mediaPlayer.Open(notification.AudioUri);
                    }
                    _mediaPlayer.Play();
                }

                makeTextures(notification.DisplayText, settings);

                // cycle through frames in case we got a gif
                var framesIteratorThread = new Thread(framesIteratorTask);
                framesIteratorThread.Start();

                // animation
                fadeIn();
                Thread.Sleep(5000);
                fadeOut();
                Thread.Sleep(1000);

                framesIteratorThread.Abort();

                if(notification.PlayAudioToEnd) // we want to wait with the next notification until playback completed
                    Thread.Sleep(_mediaPlayer.NaturalDuration.TimeSpan.Subtract(_mediaPlayer.Position));
            }
        }

        private void fadeOut()
        {
            for (var i = 0x0F; i >= 0; i--)
            {
                _currentOpacity = i * 0x10;
                Thread.Sleep(16);
            }
        }

        private void fadeIn()
        {
            for (var i = 0; i <= 0x0F; i++)
            {
                _currentOpacity = i * 0x10;
                Thread.Sleep(16);
            }
        }

        private void makeTextures(string username, Settings settings)
        {
            if (_settings == null) return;

            lock (_textureLock)
            {
                _textures.Dispose();
                UpdateSize(settings);
                using (var drawing = draw(username, settings))
                {
                    _templateFrames = loadImage(settings.ImageFile);

                    // ReSharper disable once ForCanBeConvertedToForeach
                    for (var i = 0; i < _templateFrames.Count; i++)
                    {
                        var wb = new WriteableBitmap((int)Size.X, (int)Size.Y, 96, 96, PixelFormats.Bgra32, null);
                        Chubs_BitBltMerge(ref wb, 0, 0, _templateFrames[i]);

                        _currentFrame = (_currentFrame + 1) % _templateFrames.Count;

                        var currentImage = sourceToImage(Imaging.CreateBitmapSourceFromHBitmap(drawing.GetHbitmap(),
                            IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));

                        Chubs_BitBltMerge(ref wb, 0, 0, currentImage);
                        var texture = GS.CreateTexture((uint)Size.X, (uint)Size.Y,
                            GSColorFormat.GS_BGRA, null, false, false);
                        texture.SetImage(wb.BackBuffer, GSImageFormat.GS_IMAGEFORMAT_BGRA, (uint)(wb.PixelWidth * 4));
                        _textures.Add(texture);
                    }
                }
            }
        }

        public void Chubs_BitBltMerge(ref WriteableBitmap dest, int nXDest, int nYDest, BitmapImage src)
        {
            // copy the source image into a byte buffer
            var srcStride = src.PixelWidth * (src.Format.BitsPerPixel >> 3);
            var srcBuffer = new byte[srcStride * src.PixelHeight];
            src.CopyPixels(srcBuffer, srcStride, 0);

            // copy the dest image into a byte buffer
            var destStride = src.PixelWidth * (dest.Format.BitsPerPixel >> 3);
            var destBuffer = new byte[(src.PixelWidth * src.PixelHeight) << 2];
            dest.CopyPixels(new Int32Rect(nXDest, nYDest, src.PixelWidth, src.PixelHeight), destBuffer, destStride, 0);

            // do merge (could be made faster through parallelization)
            for (var i = 0; i < srcBuffer.Length; i = i + 4)
            {
                var srcAlpha = (float)srcBuffer[i + 3] / 255;
                var maxedAlpha = Math.Max(srcBuffer[i + 3], destBuffer[i + 3]);
                destBuffer[i + 0] = (byte)(srcBuffer[i + 0] * srcAlpha + (destBuffer[i + 3] > 0 ? destBuffer[i + 0] : 0) * (1.0 - srcAlpha));
                destBuffer[i + 1] = (byte)(srcBuffer[i + 1] * srcAlpha + (destBuffer[i + 3] > 0 ? destBuffer[i + 1] : 0) * (1.0 - srcAlpha));
                destBuffer[i + 2] = (byte)(srcBuffer[i + 2] * srcAlpha + (destBuffer[i + 3] > 0 ? destBuffer[i + 2] : 0) * (1.0 - srcAlpha));
                destBuffer[i + 3] = maxedAlpha;
            }

            // copy dest buffer back to the dest WriteableBitmap
            dest.WritePixels(new Int32Rect(nXDest, nYDest, src.PixelWidth, src.PixelHeight), destBuffer, destStride, 0);
        }

        private static List<BitmapImage> loadImage(string filename)
        {
            var bitmapList = new List<BitmapImage>();
            if (!filename.ToLower().EndsWith(".gif"))
            {
                var currentImage = new BitmapImage();

                currentImage.BeginInit();
                currentImage.UriSource = new Uri(filename);
                currentImage.EndInit();

                bitmapList.Add(currentImage);
            }
            else
            {
                var image = Image.FromFile(filename);
                for (var i = 0; i < image.GetFrameCount(FrameDimension.Time); i++)
                {
                    image.SelectActiveFrame(FrameDimension.Time, i);
                    using (var memory = new MemoryStream())
                    {
                        image.Save(memory, ImageFormat.Png);
                        memory.Position = 0;
                        var bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = memory;
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.EndInit();

                        bitmapList.Add(bitmapImage);
                    }
                }
            }

            return bitmapList;
        }

        public void UpdateSettings(Settings settings)
        {
            _settings = settings;
            UpdateSize(settings);
        }

        public void UpdateSize(Settings settings)
        {
            //lock (_textureLock)
            {
                using (var img = Image.FromFile(settings.ImageFile))
                {
                    var rect = settings.TextRect;
                    rect.Inflate(10, 10);
                    Size.X = Math.Max(img.Size.Width, rect.X + rect.Width);
                    Size.Y = Math.Max(img.Size.Height, rect.Y + rect.Height);
                }
            }
        }

        private Bitmap draw(string displayText, Settings settings)
        {
            var drawing = new Bitmap((int)Size.X, (int)Size.Y, PixelFormat.Format32bppPArgb);

            using (var fontCollection = new PrivateFontCollection())
            {
                fontCollection.AddFontFile(settings.FontFile);
                var displayFont = new Font(fontCollection.Families[0], settings.FontSize);

                using (var g = Graphics.FromImage(drawing))
                {
                    var textBoxSize = g.MeasureString(displayText, displayFont, settings.TextRect.Size);
                    var textBoxRect = settings.TextRect;
                    textBoxRect.Inflate(10, 10);
                    textBoxRect.Width = (int)textBoxSize.Width + 20;
                    g.FillRectangle(new SolidBrush(settings.TextBackground.ToGdiColor()), textBoxRect);

                    g.DrawString(displayText, displayFont, new SolidBrush(settings.TextColor.ToGdiColor()),
                        settings.TextRect);
                }
                return drawing;
            }
        }

        private static BitmapImage sourceToImage(BitmapSource bitmapSource)
        {
            var encoder = new PngBitmapEncoder();
            var memoryStream = new MemoryStream();
            var bitmapImage = new BitmapImage();

            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            encoder.Save(memoryStream);

            memoryStream.Position = 0;
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();
            memoryStream.Close();

            bitmapImage.Freeze();

            return bitmapImage;
        }

        public void Preview(Notification notification, Settings settings)
        {
            notify(notification, settings);
        }
    }
}