﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using baconworx.Mollyboter.ObsPlugin.Models;
using baconworx.Mollyboter.ObsPlugin.ViewModels;
using MessageBox = System.Windows.MessageBox;

namespace baconworx.Mollyboter.ObsPlugin
{
    public partial class ConfigurationDialog
    {
        private const string IMAGE_FILES_FILTER = "Image Files|*.jpeg;*.png;*.jpg;*.gif|All Files|*.*";
        private const string FONT_FILES_FILTER = "TrueType Font Files (*.ttf)|*.ttf";
        private const string AUDIO_FILES_FILTER = "Audio Files|*.mp3;*.wav;*.wma;*.aac|All Files|*.*";

        private const string OAUTH_TOKEN_GENERATOR_ENDPOINT = "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=qma4wgx8bdqqghebpwj3a70y86f6fyb&redirect_uri=http://mollyboter.blackforest.at/Oauth&scope=chat_login+channel_check_subscription";

        private readonly MollyboterPlugin _plugin;

        private TextBox _imageFileTextBox, _fontFileTextBox, _audioUriTextBox;
        private Button _oauthTokenButton, _imageFileChooserButton, _fontFileChooserButton, _resetCooldownsButton, _showIrcLogButton, _audioUriChooserButton;

        private ListView _displayTriggersListView;
        private Button _displayTriggerNewButton, _displayTriggerDeleteButton;

        private DisplayTriggerViewModel _selectedDisplayTrigger;

        private Button _previewButton;
        private TextBlock _previewUnavailableTextBlock;

        private readonly Settings _settings, _settingsCopy;
        private readonly SettingsViewModel _viewModel;
        private readonly MollyboterImageSource _imageSource;
        private ObservableFixedSizedQueue<string> _ircLog;

        public ConfigurationDialog(MollyboterPlugin plugin, Settings settings)
        {
            _plugin = plugin;
            _settings = settings ?? new Settings();
            _viewModel = new SettingsViewModel(_settingsCopy = _settings.Clone());

            _imageSource = plugin.MollyboterImageSourceFactory.ImageSource;

            DataContext = _viewModel;

            InitializeComponent();

            registerComponents();
            registerEvents();
            setupUi();
        }

        protected override void OnOkButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            _settings.Channel = _settingsCopy.Channel;
            _settings.OauthToken = _settingsCopy.OauthToken;
            _settings.Channel = _settingsCopy.Channel;
            _settings.OauthToken = _settingsCopy.OauthToken;
            _settings.ImageFile = _settingsCopy.ImageFile;
            _settings.Fps = _settingsCopy.Fps;
            _settings.TextRect = _settingsCopy.TextRect;
            _settings.FontSize = _settingsCopy.FontSize;
            _settings.TextColor = _settingsCopy.TextColor;
            _settings.TextBackground = _settingsCopy.TextBackground;
            _settings.FontFile = _settingsCopy.FontFile;
            _settings.UserCooldown = _settingsCopy.UserCooldown;
            _settings.DisplayTriggers = _settingsCopy.DisplayTriggers;

            base.OnOkButtonClick(sender, routedEventArgs);
        }

        private void registerComponents()
        {
            _oauthTokenButton = (Button)FindName("OauthTokenButton");

            _imageFileTextBox = (TextBox)FindName("ImageFileTextBox");
            _imageFileChooserButton = (Button)FindName("ImageFileChooserButton");
            _fontFileTextBox = (TextBox)FindName("FontFileTextBox");
            _audioUriTextBox = (TextBox)FindName("AudioUriTextBox");
            _fontFileChooserButton = (Button)FindName("FontFileChooserButton");
            _resetCooldownsButton = (Button)FindName("ResetCooldownsButton");
            _audioUriChooserButton = (Button)FindName("AudioUriChooserButton");

            _displayTriggersListView = (ListView)FindName("DisplayTriggersListView");
            _displayTriggerNewButton = (Button)FindName("DisplayTriggerNewButton");
            _displayTriggerDeleteButton = (Button)FindName("DisplayTriggerDeleteButton");

            _previewButton = (Button)FindName("PreviewButton");
            _previewUnavailableTextBlock = (TextBlock)FindName("PreviewUnavailableTextBlock");
            _showIrcLogButton  = (Button)FindName("ShowIrcLogButton");
        }

        private void registerEvents()
        {
            _oauthTokenButton.Click += (sender, args) => System.Diagnostics.Process.Start(OAUTH_TOKEN_GENERATOR_ENDPOINT);

            _imageFileChooserButton.Click += (sender, args) =>
            {
                var filepath = ChooseFile(".png", IMAGE_FILES_FILTER);
                if (string.IsNullOrEmpty(filepath))
                    return;

                _viewModel.ImageFile = filepath;
                _imageFileTextBox.GetBindingExpression(TextBox.TextProperty)?.UpdateTarget();
            };

            _fontFileChooserButton.Click += (sender, args) =>
            {
                var filepath = ChooseFile(".ttf", FONT_FILES_FILTER);
                if (string.IsNullOrEmpty(filepath))
                    return;

                _viewModel.FontFile = filepath;
                _fontFileTextBox.GetBindingExpression(TextBox.TextProperty)?.UpdateTarget();
            };

            _displayTriggersListView.SelectionChanged += (sender, args) =>
            {
                _selectedDisplayTrigger = args.AddedItems.Count > 0 ? args.AddedItems[0] as DisplayTriggerViewModel : null;
                _previewButton.IsEnabled = args.AddedItems.Count > 0;
            };

            _resetCooldownsButton.Click += (sender, args) =>
            {
                if(confirmDialog("Reset cooldowns?", "Please confirm"))
                    _plugin.ResetCooldowns();
            };

            _showIrcLogButton.Click += (sender, args) =>
            {
                new IrcLogDialog(_plugin.MollyboterImageSourceFactory.ChatLog).Show();
            };

            _displayTriggerNewButton.Click += (sender, args) => newDisplayTrigger();
            _displayTriggerDeleteButton.Click += (sender, args) => deleteSelectedDisplayTrigger();

            _previewButton.Click += (sender, args) =>
            {
                _previewButton.IsEnabled = false;
                Task.Run(() =>
                {
                    _imageSource.Preview(new Notification(_selectedDisplayTrigger.DisplayPattern), _settingsCopy);
                    Dispatcher.Invoke(() => { _previewButton.IsEnabled = _displayTriggersListView.SelectedItem != null; });
                });
            };

            _audioUriChooserButton.Click += (sender, args) =>
            {
                if (_selectedDisplayTrigger == null)
                    return;

                var filepath = ChooseFile(".mp3", AUDIO_FILES_FILTER);
                if (string.IsNullOrEmpty(filepath))
                    return;

                _selectedDisplayTrigger.AudioUri = new Uri(filepath);
                _audioUriTextBox.GetBindingExpression(TextBox.TextProperty)?.UpdateTarget();
            };
        }

        private bool confirmDialog(string text, string caption)
        {
            return MessageBox.Show(text, caption, MessageBoxButton.YesNo) == MessageBoxResult.Yes;
        }

        private void setupUi()
        {
            _previewButton.Visibility = _imageSource != null ? Visibility.Visible : Visibility.Collapsed;
            _previewUnavailableTextBlock.Visibility = _imageSource != null ? Visibility.Collapsed : Visibility.Visible;
        }

        private void newDisplayTrigger()
        {
            var newTrigger = _viewModel.AddTrigger();
            _displayTriggersListView.Items.Refresh();
            _displayTriggersListView.SelectedItem = newTrigger;
        }

        private void deleteSelectedDisplayTrigger()
        {
            var selectedTrigger = _displayTriggersListView.SelectedItem as DisplayTriggerViewModel;
            if (selectedTrigger == null)
                return;

            _viewModel.RemoveTrigger(selectedTrigger);
            _displayTriggersListView.Items.Refresh();
        }
    }
}
